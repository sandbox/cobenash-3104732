<?php

namespace Drupal\commerce_text_pane\Plugin\commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Custom Ksnews Shopping pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_text_pane",
 *   label = @Translation("Custom Text Pane"),
 * )
 */
class CustomTextPane extends CheckoutPaneBase {

  /**
   * Default Configuration.
   */
  public function defaultConfiguration() {
    return [
      'custom_text' => 'You can add custom text you want.',
    ] + parent::defaultConfiguration();
  }

  /**
   * Some comment need to finish.
   */
  public function buildConfigurationSummary() {
    return $this->t('You can add custom text you want.');
  }

  /**
   * Some comment need to finish.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['custom_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Custom Text'),
      '#format' => 'full_html',
      '#default_value' => $this->configuration['custom_text']['value'],
    ];

    return $form;
  }

  /**
   * Some comment need to finish.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['custom_text'] = $values['custom_text'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['custom_text'] = [
      '#markup' => $this->configuration['custom_text']['value'],
    ];
    return $pane_form;
  }

}
